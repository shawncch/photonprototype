
#include "LoadBalancingListener.h"
#include "PhotonPrototype.h"
#include "DemoConstants.h"
using namespace ExitGames::Common;
using namespace ExitGames::LoadBalancing;

static int randomColor(int from = 0, int to = 256) 
{
	int r = from + rand() % (to - from);
	int g = from + rand() % (to - from);
	int b = from + rand() % (to - from);
	return (r << 16) + (g << 8) + b;
}

const char* PeerStatesStr[] =
{
	"Uninitialized",
	"PeerCreated",
	"ConnectingToNameserver",
	"ConnectedToNameserver",
	"DisconnectingFromNameserver",
	"Connecting",
	"Connected",
	"WaitingForCustomAuthenticationNextStepCall",
	"Authenticated",
	"JoinedLobby",
	"DisconnectingFromMasterserver",
	"ConnectingToGameserver",
	"ConnectedToGameserver",
	"AuthenticatedOnGameServer",
	"Joining",
	"Joined",
	"Leaving",
	"Left",
	"DisconnectingFromGameserver",
	"ConnectingToMasterserver",
	"ConnectedComingFromGameserver",
	"AuthenticatedComingFromGameserver",
	"Disconnecting",
	"Disconnected"
};

// TODO: update PeerStatesStr when PeerStates changes
// Checker below checks count match only
class PeerStatesStrChecker
{
public:
	PeerStatesStrChecker(){(sizeof(PeerStatesStr)/sizeof(const char*) == PeerStates::Disconnected + 1);}
} checker;

LocalPlayer::LocalPlayer() : x(0), y(0), color(randomColor(100)), lastUpdateTime(0) {}

LoadBalancingListener::LoadBalancingListener(BaseView* view): 
	mLbc(NULL), 
	mView(view), 
	mLocalPlayerNr(0), 
	mGridSize(DEFAULT_GRID_SIZE), 
	mMap(DEFAULT_MAP), 
	mAutomove(true), 
	mUseGroups(true), 
	mSendGroup(0) 
{
}

void LoadBalancingListener::debugReturn(const JString& string)
{
	mView->info(string.UTF8Representation().cstr());
}

void LoadBalancingListener::debugReturn(int debugLevel, const JString& string)
{
	const char* s = string.UTF8Representation().cstr();
	switch (debugLevel)
	{
	case DebugLevel::ERRORS:
		mView->error(s); break;
	case DebugLevel::WARNINGS:
		mView->warn(s); break;
	default:
		mView->info(s); break;
	}
}

void LoadBalancingListener::connectionErrorReturn(int errorCode)
{
	mView->warn("connection failed with error %d", errorCode);
	updateState();
	// reconnect on error
//	connect();
}

void LoadBalancingListener::clientErrorReturn(int errorCode)
{
	mView->warn("received error %d from client", errorCode);
	updateState();
}

void LoadBalancingListener::warningReturn(int warningCode)
{
	mView->warn("received warning %d from client", warningCode);
}

void LoadBalancingListener::serverErrorReturn(int errorCode)
{
	mView->warn("received error %d from server", errorCode);
	updateState();
}

void LoadBalancingListener::joinRoomEventAction(int playerNr, const JVector<int>& playernrs, const Player& player)
{
	mView->info("player %d %s has joined the game", playerNr, player.getName().UTF8Representation().cstr());
	mView->addPlayer(playerNr, player.getName().UTF8Representation().cstr(), player.getNumber() == mLbc->getLocalPlayer().getNumber());
	 
	//mView->changePlayerColor(mLocalPlayerNr, mLocalPlayer.color);
	//updateGroups();
}

void LoadBalancingListener::leaveRoomEventAction(int playerNr, bool isInactive)
{
	if(isInactive)
		mView->info( "player %d has suspended the game", playerNr);		
	else
	{
		mView->info( "player %d has abandoned the game", playerNr);
		mView->removePlayer(playerNr);
	}
}

void LoadBalancingListener::disconnectEventAction(int playerNr)
{
	mView->info( "player %d has disconnected", playerNr);
}

void LoadBalancingListener::customEventAction(int playerNr, nByte eventCode, const Object& eventContentObj)
{
	//local playerObj = scene.getTaggedObject ( application.getCurrentUserScene ( ), "" .. playerNr )
	//	view->info("event %d: %s", eventCode, eventContent.toString(true).UTF8Representation().cstr());
	ExitGames::Common::Hashtable eventContent = ExitGames::Common::ValueObject<ExitGames::Common::Hashtable>(eventContentObj).getDataCopy();
 
	if (playerNr == 0)
		playerNr = 1;
 
	Object const* forwardObj = eventContent.getValue((float)1);
		Object const* rightObj = eventContent.getValue((float)2);
		Object const* posObj = eventContent.getValue((int)3);
		 
		if (eventCode > 0) {
			float* forwardData = ((ValueObject<float*>*)forwardObj)->getDataCopy();
			float fAxis = (float)forwardData[0];
			float fx = (float)forwardData[1];
			float fy = (float)forwardData[2];
			float fz = (float)forwardData[3];

			float* rightData = ((ValueObject<float*>*)rightObj)->getDataCopy();
			float rAxis = (float)rightData[0];
			float rx = (float)rightData[1];
			float ry = (float)rightData[2];
			float rz = (float)rightData[3];
			float yaw = (float)rightData[4];

			mView->changePlayerAction(playerNr, eventCode, fAxis, fx, fy, fz, rAxis, rx, ry, rz, yaw);

		}
		else {
			int x = 0; int y = 0; int z = 0;
			int* posData = ((ValueObject<int*>*)posObj)->getDataCopy();
			x = (int)posData[0];
			y = (int)posData[1];
			z = (int)posData[2];
			mView->changePlayerPos(playerNr, x, y, z);
		}
	 

 
	 
 /*
	if(eventCode == 1)
	{
		Object const* obj = eventContent.getValue("1");
		if(obj == NULL) obj = eventContent.getValue((nByte)1);
		if(obj == NULL) obj = eventContent.getValue(1);
		if(obj == NULL) obj = eventContent.getValue(1.0);
		if(obj) 
		{
			int color = (int)(obj->getType() == 'd' ?  ((ValueObject<double>*)obj)->getDataCopy() : ((ValueObject<int>*)obj)->getDataCopy());
			mView->changePlayerColor(playerNr, color);
		}
		else
			mView->warn( "bad color event");
	}
	else if(eventCode == 2)
	{
		Object const* obj = eventContent.getValue("1");
		if(obj == NULL) obj = eventContent.getValue((nByte)1);
		if(obj == NULL) obj = eventContent.getValue(1);
		if(obj == NULL) obj = eventContent.getValue(1.0);
		if(obj && obj->getDimensions() == 1 && obj->getSizes()[0] == 2) 
		{
			int x = 0; int y = 0;
			if(obj->getType() == 'd') 
			{
				double* data = ((ValueObject<double*>*)obj)->getDataCopy();
				x = (int)data[0];
				y = (int)data[1];
			}
			if(obj->getType() == 'i') 
			{
				int* data = ((ValueObject<int*>*)obj)->getDataCopy();
				x = (int)data[0];
				y = (int)data[1];
			}
			else if(obj->getType() == 'b') 
			{
				nByte* data = ((ValueObject<nByte*>*)obj)->getDataCopy();
				x = (int)data[0];
				y = (int)data[1];
			}
			else if(obj->getType() == 'z') 
			{
				Object* data = ((ValueObject<Object*>*)obj)->getDataCopy();
				if(data[0].getType() == 'i') 
				{
					x = ((ValueObject<int>*)(data + 0))->getDataCopy();
					y = ((ValueObject<int>*)(data + 1))->getDataCopy();
				}
				else 
				{
					x = (int)((ValueObject<double>*)(data + 0))->getDataCopy();
					y = (int)((ValueObject<double>*)(data + 1))->getDataCopy();
				}
			}
			mView->changePlayerPos(playerNr, x, y);
 
		}
		else 
			mView->warn( "Bad position event");
	}
	*/
	 
}

void LoadBalancingListener::connectReturn(int errorCode, const JString& errorString, const JString& cluster)
{
	updateState();
	if(errorCode == ErrorCode::OK)
	{
		mView->info("connected to cluster %s", cluster.UTF8Representation().cstr());
		mView->info(cluster.UTF8Representation().cstr());
		//mLbc->opJoinRandomRoom();
	}
	else
		mView->error("Warn: connect failed %d %s", errorCode, errorString.UTF8Representation().cstr());
}

void LoadBalancingListener::disconnectReturn(void)
{
	updateState();
	mView->info("disconnected");
	mView->initPlayers();
}

void LoadBalancingListener::createRoomReturn(int localPlayerNr, const Hashtable& gameProperties, const Hashtable& playerProperties, int errorCode, const JString& errorString)
{
	updateState();
	if(errorCode == ErrorCode::OK)
	{
		mView->info("room has been created");
		//afterRoomJoined(localPlayerNr);
	}
	else
		mView->error("Warn: opCreateRoom() failed: %s", errorString.UTF8Representation().cstr() );
}

void LoadBalancingListener::joinOrCreateRoomReturn(int localPlayerNr, const Hashtable& gameProperties, const Hashtable& playerProperties, int errorCode, const JString& errorString)
{
	updateState();
	if (errorCode == ErrorCode::OK)
	{
		mView->info("room has been entered");
		afterRoomJoined(localPlayerNr);
	}
	else
		mView->error("Warn: opJoinOrCreateRoom() failed: %s", errorString.UTF8Representation().cstr());
}

void LoadBalancingListener::joinRoomReturn(int localPlayerNr, const Hashtable& gameProperties, const Hashtable& playerProperties, int errorCode, const JString& errorString)
{
	updateState();
	if(errorCode == ErrorCode::OK)
	{
		mView->info("game room has been successfully joined");
		afterRoomJoined(localPlayerNr);
	}
	else
		mView->error("opJoinRoom() failed: %s", errorString.UTF8Representation().cstr());
}

void LoadBalancingListener::joinRandomRoomReturn(int localPlayerNr, const Hashtable& gameProperties, const Hashtable& playerProperties, int errorCode, const JString& errorString)
{
	updateState();
	if(errorCode == ErrorCode::NO_MATCH_FOUND)
		createRoom();
	else if(errorCode == ErrorCode::OK)
	{
		mView->info("game room has been successfully joined");
		afterRoomJoined(localPlayerNr);
	}
	else
		mView->error("opJoinRandomRoom() failed: %s", errorString.UTF8Representation().cstr());
}

void LoadBalancingListener::leaveRoomReturn(int errorCode, const JString& errorString)
{
	updateState();
	if(errorCode == ErrorCode::OK)
		mView->info("game room has been successfully left");
	else
		mView->error("opLeaveRoom() failed: %s", errorString.UTF8Representation().cstr());
	mView->initPlayers();
}

void LoadBalancingListener::gotQueuedReturn(void)
{
	updateState();
}

void LoadBalancingListener::joinLobbyReturn(void)
{
	mView->info("joined lobby");
	updateState();
	mLbc->opJoinRandomRoom();
}

void LoadBalancingListener::leaveLobbyReturn(void)
{
	mView->info("left lobby");
	updateState();
}

void LoadBalancingListener::onLobbyStatsUpdate(const JVector<LobbyStatsResponse>& res)
{
	mView->info("===================== lobby stats update");
	for(unsigned int i = 0;i < res.getSize();++i) 
		mView->info(res[i].toString().UTF8Representation().cstr());

	// lobby stats request test
	JVector<LobbyStatsRequest> ls;
	ls.addElement(LobbyStatsRequest());
	ls.addElement(LobbyStatsRequest(L"AAA"));
	ls.addElement(LobbyStatsRequest(L"BBB"));
	ls.addElement(LobbyStatsRequest(L"CCC", LobbyType::DEFAULT));
	ls.addElement(LobbyStatsRequest(L"AAA", LobbyType::SQL_LOBBY));
	mLbc->opLobbyStats(ls);
//	lbc->opLobbyStats();
}

void LoadBalancingListener::onLobbyStatsResponse(const ExitGames::Common::JVector<LobbyStatsResponse>& res)
{
	mView->info("===================== lobby stats response");
	for(unsigned int i = 0;i < res.getSize();++i) 
		mView->info(res[i].toString().UTF8Representation().cstr());
}

void LoadBalancingListener::onRoomListUpdate()
{	
	const JVector<Room*>& rooms = mLbc->getRoomList();
	const char** names = MemoryManagement::allocateArray<const char*>(rooms.getSize());
	for(unsigned int i = 0; i < rooms.getSize();++i) 
	{
		const Room* r = rooms.getElementAt(i);
		UTF8String u = r->getName();
		const char* name = u.cstr();
		char* tmp = MemoryManagement::allocateArray<char>(strlen(name) + 1);
		strcpy(tmp, name);
		names[i] = tmp;
	}
	mView->updateRoomList(names, rooms.getSize());
	for(unsigned int i = 0; i < rooms.getSize();++i) 
		MemoryManagement::deallocateArray(names[i]);
	MemoryManagement::deallocateArray(names);
}

void LoadBalancingListener::onRoomPropertiesChange(const ExitGames::Common::Hashtable& changes) 
{
	if(updateGridSize(changes)) 
		mView->setupScene(mGridSize);
}

void LoadBalancingListener::updateState() 
{
	int state = mLbc->getState();
	mView->updateState(state, PeerStatesStr[state], 
		state == PeerStates::Joined ? mLbc->getCurrentlyJoinedRoom().getName().UTF8Representation().cstr() : NULL);
}

bool LoadBalancingListener::updateGridSize(const ExitGames::Common::Hashtable& props) 
{
	if(props.contains("s")) 
	{
		const Object* v = props.getValue("s");
		char x = v->getType();
		switch(v->getType()) 
		{
			case 'i':
					mGridSize = ((ValueObject<int>*)v)->getDataCopy();
					return true;
			case 'd':
					mGridSize = (int)((ValueObject<double>*)v)->getDataCopy();
					return true;
		}
	}
	return false;
}

void LoadBalancingListener::afterRoomJoined(int localPlayerNr)
{
	 
	this->mLocalPlayerNr = localPlayerNr;
	MutableRoom& myRoom = mLbc->getCurrentlyJoinedRoom();
	Hashtable props = myRoom.getCustomProperties();
	updateGridSize(props);
	if(props.contains("m"))
		mMap = ((ValueObject<JString>*)props.getValue("m"))->getDataCopy();

	mView->initPlayers();
	//mView->setupScene(mGridSize);
	
	const JVector<Player*>& players = myRoom.getPlayers();
	int players_size = players.getSize();
	mView->info("afterRoomJoined: players size= %d", players_size);
	 
	 
	for(unsigned int i=0; i<players.getSize(); ++i)
	{
	 
		const Player* p = players[i];
		if(p->getNumber() != localPlayerNr)
		mView->addPlayer(p->getNumber(), p->getName().UTF8Representation().cstr(), p->getNumber() == mLbc->getLocalPlayer().getNumber());
	}
	//mView->changePlayerColor(localPlayerNr, mLocalPlayer.color);
	//raiseColorEvent();
}

void LoadBalancingListener::createRoom() 
{
	char name[16];
	sprintf(name, "UE-%d", GETTIMEMS());
	Hashtable props;
	props.put("s", mGridSize);
	props.put("m", mMap);
	if(mLbc->opCreateRoom(name, ExitGames::LoadBalancing::RoomOptions().setCustomRoomProperties(props)))
		mView->info("Creating room %s", name);
	else
		mView->error("Can't create room in current state");
}

void LoadBalancingListener::leave()
{
	if(!mLbc->opLeaveRoom()) 
		mView->error("Not in room currently");
}

void LoadBalancingListener::service()
{
	
	unsigned long t = GETTIMEMS();
	if((t - mLocalPlayer.lastUpdateTime) > PLAYER_UPDATE_INTERVAL_MS) 
	{
		mLocalPlayer.lastUpdateTime = t;
		//if(mLbc->getState() == PeerStates::Joined && mAutomove) 
			//moveLocalPlayer();
	}
}

void LoadBalancingListener::moveLocalPlayer()
{
/*
int prevGroup = getGroupByPos();
	int d = rand() % 8;
	int xOffs[] = {-1, 0, 1, -1, 1, -1, 0, 1};
	int yOffs[] = {1, 1, 1, 0, 0, -1, -1, -1};
	int x = mLocalPlayer.x + xOffs[d];
	int y = mLocalPlayer.y += yOffs[d];
	if(x < 0) x = 1;
	if(x >= mGridSize) x = mGridSize - 2;
	if(y < 0) y = 1;
	if(y >= mGridSize) y = mGridSize - 2;
	setLocalPlayerPos(x, y);
	
	if(prevGroup != getGroupByPos()) 
		updateGroups();
		*/
}

bool LoadBalancingListener::setLocalPlayerPos(int localPlayerNr, int x, int y, int z)
{
	mLocalPlayer.x = x;
	mLocalPlayer.y = y;
	mLocalPlayer.z = z;
	Hashtable data;
	int coords[] = { mLocalPlayer.x, mLocalPlayer.y, mLocalPlayer.z };
	data.put((int)3, coords, 3);
	if (localPlayerNr == 0)
		localPlayerNr = 1;

	mLbc->opRaiseEvent(false, data, 0);
	//mView->changePlayerPos(localPlayerNr, mLocalPlayer.x, mLocalPlayer.y, mLocalPlayer.z);
	return true;

	/*
	if(x >= 0 && x < mGridSize && y >= 0 && y < mGridSize && (mLocalPlayer.x != x || mLocalPlayer.y != y))
	{
		mLocalPlayer.x = x;
		mLocalPlayer.y = y;
		Hashtable data;
		nByte coords[] = {mLocalPlayer.x, mLocalPlayer.y};
		data.put((nByte)1, coords, 2);

		if (mSendGroup)
			mLbc->opRaiseEvent(false, data, 2, ExitGames::LoadBalancing::RaiseEventOptions().setInterestGroup(mSendGroup));
		else
		{
			if (mUseGroups)
				mLbc->opRaiseEvent(false, data, 2, ExitGames::LoadBalancing::RaiseEventOptions().setInterestGroup(getGroupByPos()));
			else
				mLbc->opRaiseEvent(false, data, 2);
		}
		 
		mView->changePlayerPos(mLocalPlayerNr, mLocalPlayer.x, mLocalPlayer.y);
		return true;
	}
	else 
		return false;
		*/
}

void LoadBalancingListener::updateLocalPlayer(int localPlayerNr, int action, float forwardAxis, float fx, float fy, float fz, float rightAxis, float rx, float ry, float rz, float yaw)
{
	 
	Hashtable data;
	float forwardVS[] = { forwardAxis,fx,fy,fz };
	data.put((float)1, forwardVS,4);
	float rightVS[] = { rightAxis,rx,ry,rz,yaw };
	data.put((float)2, rightVS,5);

	nByte eventCode = action;
	if (localPlayerNr == 0)
		localPlayerNr = 1;
	
	mLbc->opRaiseEvent(false, data, eventCode);
	//mView->changePlayerAction(localPlayerNr, action);
}

void LoadBalancingListener::raiseColorEvent()
{
	Hashtable data;	
	data.put((nByte)1, mLocalPlayer.color);
	mLbc->opRaiseEvent(true, data, 1, ExitGames::LoadBalancing::RaiseEventOptions().setEventCaching(ExitGames::Lite::EventCache::ADD_TO_ROOM_CACHE));
}

int LoadBalancingListener::getGroupByPos(void)
{
	int xp = mLocalPlayer.x * GROUPS_PER_AXIS / mGridSize;
	int yp = mLocalPlayer.y * GROUPS_PER_AXIS / mGridSize;
	return 1 + xp + yp * GROUPS_PER_AXIS;
}

void LoadBalancingListener::changeRandomColor()
{
	mLocalPlayer.color = randomColor(100);
	raiseColorEvent();
	mView->changePlayerColor(mLocalPlayerNr, mLocalPlayer.color);
}

void LoadBalancingListener::nextGridSize()
{
	mGridSize = mGridSize << 1;
	if(mGridSize > GRID_SIZE_MAX) 
		mGridSize = GRID_SIZE_MIN;
	mView->info("nextGridSize: %d", mGridSize);
	mLbc->getCurrentlyJoinedRoom().addCustomProperty("s", mGridSize);
	mView->setupScene(mGridSize);
}

void LoadBalancingListener::updateGroups()
{
	if (mLbc->getIsInRoom())
	{	
		ExitGames::Common::JVector<nByte> remove;
		if(mUseGroups)
		{
			ExitGames::Common::JVector<nByte> add;
			add.addElement(getGroupByPos());
			mLbc->opChangeGroups(&remove, &add);
		}
		else
			mLbc->opChangeGroups(&remove, NULL);
	}
}