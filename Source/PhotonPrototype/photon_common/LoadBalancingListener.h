
#pragma once

// the only photon include point
//#include "LoadBalancing-cpp/inc/Client.h"
#include "photon-import.h"

#include "BaseView.h"

struct LocalPlayer
{
	LocalPlayer();
	nByte x;
	nByte y;
	nByte z;
	int color;
	unsigned long lastUpdateTime;
};
class LoadBalancingListener : public ExitGames::LoadBalancing::Listener
{
	ExitGames::LoadBalancing::Client* mLbc;
	BaseView* mView;
public: 
	LoadBalancingListener(BaseView* view);
	~LoadBalancingListener()  
	{ 
		delete mView; 
	}

	void setLBC(ExitGames::LoadBalancing::Client* lbc) 
	{
		this->mLbc = lbc;
	}
	void setUseGroups(bool value)
	{
		mUseGroups = value;
	}
	bool getUseGroups(void)
	{
		return mUseGroups;
	}
	void updateGroups();
	void setSendGroup(nByte value)
	{
		mSendGroup = value;
	}
	void service();
	void createRoom();
	void leave();
	void setAutomove(bool a) 
	{ 
		mAutomove = a; 
	}
	bool getAutomove(void) 
	{ 
		return mAutomove;
	}
	void changeRandomColor();
	void nextGridSize();
	bool setLocalPlayerPos(int localPlayerNr, int x, int y, int z);
	void updateLocalPlayer(int localPlayerNr , int action, float forwardAxis, float fx, float fy, float fz, float rightAxis, float rx, float ry, float rz, float yaw);
	void moveLocalPlayer();
private:
	LocalPlayer mLocalPlayer;
	int mLocalPlayerNr;
	int mGridSize;
	ExitGames::Common::JString mMap;
	bool mAutomove;
	bool mUseGroups;
	int mSendGroup; // forces group in outgoing events even if useGroups = false

	//From Common::BaseListener
	// receive and print out Photon datatypes debug out here
	virtual void debugReturn(const ExitGames::Common::JString& string);

	//From LoadBalancing::LoadBalancingListener
	// receive and print out Photon LoadBalancing debug out here
	virtual void debugReturn(int debugLevel, const ExitGames::Common::JString& string);

	// implement your error-handling here
	virtual void connectionErrorReturn(int errorCode);
	virtual void clientErrorReturn(int errorCode);
	virtual void warningReturn(int warningCode);
	virtual void serverErrorReturn(int errorCode);

	// events, triggered by certain operations of all players in the same room
	virtual void joinRoomEventAction(int playerNr, const ExitGames::Common::JVector<int>& playernrs, const ExitGames::LoadBalancing::Player& player);
	virtual void leaveRoomEventAction(int playerNr, bool isInactive);
	virtual void disconnectEventAction(int playerNr);
	virtual void customEventAction(int playerNr, nByte eventCode, const ExitGames::Common::Object& eventContent);

	// callbacks for operations on PhotonLoadBalancing server
	virtual void connectReturn(int errorCode, const ExitGames::Common::JString& errorString, const ExitGames::Common::JString& cluster);
	virtual void disconnectReturn(void);
	virtual void createRoomReturn(int localPlayerNr, const ExitGames::Common::Hashtable& gameProperties, const ExitGames::Common::Hashtable& playerProperties, int errorCode, const ExitGames::Common::JString& errorString);
	virtual void joinOrCreateRoomReturn(int localPlayerNr, const ExitGames::Common::Hashtable& gameProperties, const ExitGames::Common::Hashtable& playerProperties, int errorCode, const ExitGames::Common::JString& errorString);
	virtual void joinRoomReturn(int localPlayerNr, const ExitGames::Common::Hashtable& gameProperties, const ExitGames::Common::Hashtable& playerProperties, int errorCode, const ExitGames::Common::JString& errorString);
	virtual void joinRandomRoomReturn(int localPlayerNr, const ExitGames::Common::Hashtable& gameProperties, const ExitGames::Common::Hashtable& playerProperties, int errorCode, const ExitGames::Common::JString& errorString);
	virtual void leaveRoomReturn(int errorCode, const ExitGames::Common::JString& errorString);
	virtual void gotQueuedReturn(void);
	virtual void joinLobbyReturn(void);
	virtual void leaveLobbyReturn(void);
	virtual void onRoomListUpdate(void);
	virtual void onLobbyStatsUpdate(const ExitGames::Common::JVector<ExitGames::LoadBalancing::LobbyStatsResponse>& lobbyStats);
	virtual void onLobbyStatsResponse(const ExitGames::Common::JVector<ExitGames::LoadBalancing::LobbyStatsResponse>& lobbyStats);
	virtual void onRoomPropertiesChange(const ExitGames::Common::Hashtable& changes);

	void updateState(void);
	void afterRoomJoined(int localPlayerNr);
	bool updateGridSize(const ExitGames::Common::Hashtable& props);
	void raiseColorEvent(void);
	int getGroupByPos(void);
};
